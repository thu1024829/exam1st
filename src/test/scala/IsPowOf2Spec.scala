import org.scalatest.FunSuite
import Questions._
/** 20%
  * 請寫出一個判斷某數n是否為2的次方數, isPowOf2(n)
  * ex:
  * isPowOf2(2)=true
  * isPowOf2(8)=true
  * isPowOf2(14)=false
  * isPowOf2(32)=true
  * isPowOf2(36)=false
  **/
class IsPowOf2Spec extends FunSuite {
  test("isPowOf2(1)=true") {
    assert(isPowOf2(1))

  }

  test("isPowOf2(3)=false") {
    assert(!isPowOf2(3))

  }
  test("isPowOf2(36)=false") {
    assert(!isPowOf2(36))

  }
  test("isPowOf2(114688)=false") {
    assert(!isPowOf2(322))

  }
  test("isPowOf2(8388608)=true") {
    assert(isPowOf2(8388608))

  }

}
